//
//  UIColor+Hex.h
//  WebSocketManager
//
//  Created by zengqiang xing on 2023/8/16.
//

#import <UIKit/UIKit.h>

@interface UIColor (Hex)
+ (UIColor *)colorWithHexString:(NSString *)color alpha:(CGFloat)alpha;
+ (UIColor *)colorWithHexString:(NSString *)color;
@end
