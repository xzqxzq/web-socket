//
//  AppDelegate.m
//  WebSocketManager
//
//  Created by zengqiang xing on 2023/8/16.
//

#import "AppDelegate.h"
#import "WebSocketManager.h"
#import "WEDocinfoListModel.h"
#import "WESystemLocalNotiView.h"
#import "ViewController.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // 7.预加载H5网页，保留缓冲
    self.window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyWindow];
    [self.window makeKeyAndVisible];
    self.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:[ViewController new]];
    return YES;
}


- (void)getMessageForSocket:(NSNotification *)notification {
    NSDictionary *userData = notification.userInfo;
    NSLog(@"获取到的socket数据-%@",userData);
    [self testAlertView];
   
}

- (void)testAlertView {
    WEDocinfoListModel *model = [[WEDocinfoListModel alloc] init];
    model.title = @"";
    model.siteName = @"新华网";
    model.warnType = @"经营预警";
    model.tendency = -1;
    model.pubtimeStr = @"2023-08-16 18:03:23";
    dispatch_async(dispatch_get_main_queue(), ^{
        [[WESystemLocalNotiView sharedInstance] showLocalPushSetingViewWithModel:model];
    });
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[WebSocketManager shared] closeWebServiceByUser];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
     if ([WebSocketManager shared].connectType == WebSocketDisconnect || [WebSocketManager shared].connectType == WebSocketDefault) {
        [[WebSocketManager shared] connectServer];
    }
}

@end
