//
//  ViewController.m
//  WebSocketManager
//
//  Created by zengqiang xing on 2023/8/16.
//

#import "ViewController.h"
#import "UIColor+Hex.h"
#import "WEDocinfoListModel.h"
#import "WESystemLocalNotiView.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    UILabel * lab = [[UILabel alloc] initWithFrame:CGRectMake(0, 300, self.view.frame.size.width, 200)];
    lab.textColor = [UIColor colorWithHexString:@"#333333"];
    lab.font = [UIFont fontWithName:@"PingFangSC-Medium" size:26];
    lab.text = @"测试WebSocket占位页面";
    lab.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:lab];
    
    UIButton *testButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [testButton setTitle:@"点击按钮测试" forState:UIControlStateNormal];
    testButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:18];
    [testButton setTitleColor:[UIColor colorWithHexString:@"3478f6"] forState:UIControlStateNormal];
    testButton.frame = CGRectMake((self.view.frame.size.width - 180)/2.0, CGRectGetMaxY(lab.frame), 180, 40);
    testButton.layer.cornerRadius = 20.0;
    testButton.layer.masksToBounds = YES;
    testButton.layer.borderColor = [UIColor colorWithHexString:@"3478f6"].CGColor;
    testButton.layer.borderWidth = 1.0;
    [testButton addTarget:self action:@selector(testAlertView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:testButton];

}

- (void)testAlertView {
    WEDocinfoListModel *model = [[WEDocinfoListModel alloc] init];
    model.title = @"张一鸣宣布卸任字节跳动CEO 联合创始人梁汝波接任！字节跳动 张一鸣";
    model.siteName = @"新华网";
    model.warnType = @"经营预警";
    model.tendency = -1;
    model.pubtimeStr = @"2023-08-16 18:03:23";
    dispatch_async(dispatch_get_main_queue(), ^{
        [[WESystemLocalNotiView sharedInstance] showLocalPushSetingViewWithModel:model];
    });
}


@end
