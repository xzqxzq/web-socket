//
//  AppDelegate.h
//  WebSocketManager
//
//  Created by zengqiang xing on 2023/8/16.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) UIWindow *window;

@end

