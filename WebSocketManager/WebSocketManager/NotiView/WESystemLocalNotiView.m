//
//  WESystemLocalNotiView.m
//  FightingEagle
//
//  Created by zengqiang xing on 2023/6/27.
//  Copyright © 2023 wiseweb. All rights reserved.
//

#import "WESystemLocalNotiView.h"
#import "WEDocinfoListModel.h"
#import "UIColor+Hex.h"
#import <AVFoundation/AVFoundation.h>

#define kSystemLocalNotiViewHeight 97
#define kNotiTime 6

@interface WESystemLocalNotiView()

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *siteNameLabel;
@property (nonatomic, strong) UILabel *warnTypelLabel;
@property (nonatomic, strong) UILabel *tendencyLabel;
@property (nonatomic, strong) WEDocinfoListModel *model;

@end

@implementation WESystemLocalNotiView

#pragma mark - 单例

static WESystemLocalNotiView *_instance = nil;
+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken ;
    dispatch_once(&onceToken, ^{
        _instance = [[super allocWithZone:NULL] init];
        [_instance setUI];
    }) ;
    return _instance ;
}

+ (id)allocWithZone:(struct _NSZone *)zone {
    return [WESystemLocalNotiView sharedInstance];
}

- (void)setUI {
    UIView *window = [UIApplication sharedApplication].keyWindow.rootViewController.view;
    [window addSubview:self];
    self.userInteractionEnabled = YES;
    [self addGestureRecognizer];
    self.frame = CGRectMake(15, -kSystemLocalNotiViewHeight, [UIScreen mainScreen].bounds.size.width - 30, kSystemLocalNotiViewHeight);
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = 12;
    
    self.layer.shadowColor = [UIColor colorWithHexString:@"#333333" alpha:0.2].CGColor;
    self.layer.shadowOffset = CGSizeMake(0,3);
    self.layer.shadowOpacity = 1;
    self.layer.shadowRadius = 15;
    
    self.titleLabel = ({
        UILabel *label = [[UILabel alloc] init];
        label.font = [UIFont fontWithName:@"PingFangSC-Regular" size:17];
        label.textColor = [UIColor colorWithHexString:@"#333333"];
        label.numberOfLines = 2;
        label;
    });
    [self addSubview:self.titleLabel];
    
    self.siteNameLabel = ({
        UILabel *label = [[UILabel alloc] init];
        label.font = [UIFont fontWithName:@"PingFangSC-Regular" size:12];
        label.textColor = [UIColor colorWithHexString:@"#A1A1A1"];
        label;
    });
    [self addSubview:self.siteNameLabel];
    
    self.warnTypelLabel = ({
        UILabel *label = [[UILabel alloc] init];
        label.font = [UIFont fontWithName:@"PingFangSC-Regular" size:12];
        label.textColor = [UIColor colorWithHexString:@"ef0000"];
        label.layer.cornerRadius = 2.0;
        label.layer.masksToBounds = YES;
        label.layer.borderColor = [UIColor colorWithHexString:@"ef0000"].CGColor;
        label.layer.borderWidth = 0.5;
        label.textAlignment = NSTextAlignmentCenter;
        label;
    });
    [self addSubview:self.warnTypelLabel];
    
    self.tendencyLabel = ({
        UILabel *label = [[UILabel alloc] init];
        label.font = [UIFont fontWithName:@"PingFangSC-Regular" size:12];
        label.textColor = [UIColor colorWithHexString:@"ef0000"];
        label.layer.cornerRadius = 2.0;
        label.layer.masksToBounds = YES;
        label.layer.borderColor = [UIColor colorWithHexString:@"ef0000"].CGColor;
        label.layer.borderWidth = 0.5;
        label;
    });
    [self addSubview:self.tendencyLabel];
    
    
//    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(13.5);
//        make.left.mas_equalTo(13.5);
//        make.right.mas_equalTo(-13.5);
//    }];
//
//    [self.siteNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.titleLabel.mas_left);
//        make.height.mas_equalTo(20);
//        make.bottom.equalTo(self.mas_bottom).offset(-10);
//    }];
//
//    [self.warnTypelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(self.mas_right).offset(-15);
//        make.centerY.equalTo(self.siteNameLabel.mas_centerY);
//    }];
//
//    [self.tendencyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(self.warnTypelLabel.mas_left).offset(-10);
//        make.centerY.equalTo(self.siteNameLabel.mas_centerY);
//    }];
    
    self.titleLabel.frame = CGRectMake(13.5, 13.5, self.frame.size.width - 27, 35);
    self.siteNameLabel.frame = CGRectMake(13.5, CGRectGetMaxY(self.titleLabel.frame) + 5, 180, 20);
    self.warnTypelLabel.frame = CGRectMake(self.frame.size.width - 75, CGRectGetMaxY(self.titleLabel.frame) + 5, 60, 20);
    self.tendencyLabel.frame = CGRectMake(CGRectGetMinX(self.warnTypelLabel.frame) - 45, CGRectGetMaxY(self.titleLabel.frame) + 5, 35, 20);

}

/// MARK: 显示相关方法
- (void)showLocalPushSetingViewWithModel:(WEDocinfoListModel *)model {
    self.model = model;
    self.titleLabel.text = model.title ? model.title : @"消息推送";
    
    NSString *siteName = model.siteName;
    NSString *pubtime = [NSString stringWithFormat:@"%@",model.pubtimeStr];
    NSString *ts = pubtime;
    if (siteName) {
        if ([siteName isKindOfClass:[NSString class]] && siteName.length > 15) {
            siteName = [siteName substringToIndex:15];
            siteName = [siteName stringByAppendingString:@"..."];
        }
        self.siteNameLabel.text = [NSString stringWithFormat:@"%@  %@",siteName, ts];
    } else {
        self.siteNameLabel.text = [NSString stringWithFormat:@"%@", ts];
    }
    if (model.tendency == 1) {
        self.tendencyLabel.text = @" 正面 ";
        self.tendencyLabel.textColor = [UIColor colorWithHexString:@"4DA760"];
        self.tendencyLabel.layer.borderColor = [UIColor colorWithHexString:@"4DA760"].CGColor;
    } else if (model.tendency == 0) {
        self.tendencyLabel.text = @" 中性 ";
        self.tendencyLabel.layer.borderColor = [UIColor colorWithHexString:@"3478f6"].CGColor;
        self.tendencyLabel.textColor = [UIColor colorWithHexString:@"3478f6"];
    } else {
        self.tendencyLabel.text = @" 负面 ";
        self.tendencyLabel.layer.borderColor = [UIColor colorWithHexString:@"ef0000"].CGColor;
        self.tendencyLabel.textColor = [UIColor colorWithHexString:@"ef0000"];

    }
    self.warnTypelLabel.text = model.warnType ? model.warnType : @"-";
 
    
    NSInteger moveTop = 44;// kStatusBarHeight;
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    [UIView animateWithDuration:1.0 delay:0 usingSpringWithDamping:1 initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.frame = CGRectMake(self.frame.origin.x, moveTop, self.frame.size.width, self.frame.size.height);
    } completion:^(BOOL finished) {
        [self cutDownTimer];
    }];
}

- (void)dismiss{
    [UIView animateWithDuration:1.0 delay:0 usingSpringWithDamping:1 initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.frame = CGRectMake(self.frame.origin.x, -kSystemLocalNotiViewHeight, self.frame.size.width, self.frame.size.height);

    } completion:^(BOOL finished) {

    }];
}

- (void)cutDownTimer {
    __block NSInteger time = kNotiTime; // 倒计时时间
    __weak typeof(self) weakself = self;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0 * NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(time <= 0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置按钮的样式
                [weakself dismiss];
//                weakself.timeLabel.text = @"";
            });
        } else {
//            int seconds = time % 10;
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置按钮显示读秒效果
//                weakself.timeLabel.text = [NSString stringWithFormat:@"%ds",seconds];
            });
            time--;
        }
    });
    dispatch_resume(_timer);
}

#pragma mark - 手动拖动移除

- (void)addGestureRecognizer {
    UISwipeGestureRecognizer *swipeUpGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeUpGesture:)];
    swipeUpGesture.direction = UISwipeGestureRecognizerDirectionUp;
    [self addGestureRecognizer:swipeUpGesture];

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    [self addGestureRecognizer:tapGesture];

    UISwipeGestureRecognizer *swipeDownGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDownGesture:)];
    swipeDownGesture.direction = UISwipeGestureRecognizerDirectionDown;
    [self addGestureRecognizer:swipeDownGesture];
}

- (void)tapGesture:(UITapGestureRecognizer*)tapGesture{
    [self dismiss];
    
//    WEInfoDetailViewController *detailC = [[WEInfoDetailViewController alloc]init];
//    detailC.type = WEInfoDetailTypeShengyuMonitorHot;
//    detailC.isNewTopData = YES;
//    detailC.sourceType = 2;
//    WEMonitorListModel *model = [[WEMonitorListModel alloc]init];
//    model.docinfo = self.model;
//    detailC.listModel = model;
//    detailC.hidesBottomBarWhenPushed = YES;
//
//    UITabBarController *tabBarVc = (UITabBarController *)[UIApplication sharedApplication].keyWindow.rootViewController;
//    UINavigationController *nav = [tabBarVc selectedViewController];
//    [nav pushViewController:detailC animated:YES];
}

- (void)swipeUpGesture:(UISwipeGestureRecognizer*)gesture{
    if (gesture.direction == UISwipeGestureRecognizerDirectionUp) {
        [self dismiss];
    }
}

- (void)swipeDownGesture:(UISwipeGestureRecognizer*)gesture{
    if (gesture.direction == UISwipeGestureRecognizerDirectionDown) {
        [self dismiss];
    }
}

@end

