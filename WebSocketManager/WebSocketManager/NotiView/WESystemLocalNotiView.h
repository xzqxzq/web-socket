//
//  WESystemLocalNotiView.h
//  FightingEagle
//
//  Created by zengqiang xing on 2023/6/27.
//  Copyright © 2023 wiseweb. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WEDocinfoListModel;

@interface WESystemLocalNotiView : UIView

+ (instancetype)sharedInstance;
- (void)showLocalPushSetingViewWithModel:(WEDocinfoListModel *)model;
- (void)dismiss;

@end
