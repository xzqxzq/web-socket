//
//  WEDocinfoListModel.h
//  WebSocketManager
//
//  Created by zengqiang xing on 2023/8/16.
//

#import <Foundation/Foundation.h>

@interface WEDocinfoListModel : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *siteName;
@property (nonatomic, copy) NSString *warnType;
@property (nonatomic, copy) NSString *pubtimeStr;
@property (nonatomic, assign) NSInteger tendency;

@end
