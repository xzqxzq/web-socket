//
//  WebSocketManager.h
//  FightingEagle
//
//  Created by zengqiang xing on 2023/3/22.
//  Copyright © 2023 wiseweb. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger,WebSocketConnectType){
    WebSocketDefault = 0, //初始状态,未连接
    WebSocketConnect,      //已连接
    WebSocketDisconnect    //连接后断开
};

@interface WebSocketManager : NSObject

@property (nonatomic, assign)   BOOL isConnect;  //是否连接
@property (nonatomic, assign)   WebSocketConnectType connectType;

+ (instancetype)shared;
- (void)connectServer;//建立长连接
- (void)reConnectServer;//重新连接
- (void)closeWebServiceByUser;//关闭长连接
- (void)sendDataToServer:(NSString *)data;//发送数据给服务器

@end

